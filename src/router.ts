import { createWebHashHistory, createRouter } from "vue-router"
import Home from "./pages/Home.vue"
import VR from "./pages/VR.vue"

const history = createWebHashHistory()
const routes = [
    { path: "/", component: Home },
    { path: "/bosque",
      name: 'bosque',
      props: {
        title: 'Bosque',
        videoSrc: 'https://vimeo.com/527481139/c14cd1dbdb',
        audioSrc: '01'
      },
      component: VR
    },
    { path: "/cachoeira",
      name: 'cachoeira',
      props: {
        title: 'Cachoeira',
        videoSrc: 'https://vimeo.com/529574097/3fb280ca70',
        audioSrc: '02'
      },
      component: VR
    },
    { path: "/galinheiro",
      name: 'galinheiro',
      props: {
        title: 'Galinheiro',
        videoSrc: 'https://vimeo.com/529576978/b6075488af',
        audioSrc: '03'
      },
      component: VR
    },
    { path: "/galpao",
      name: 'galpao',
      props: {
        title: 'Galpão',
        videoSrc: 'https://vimeo.com/529579780/b447f8b19f',
        audioSrc: '04'
      },
      component: VR
    }
]

const router = createRouter({ history, routes })
export default router
