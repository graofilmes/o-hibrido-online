import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [vue()],
  publicDir: 'default',
  base: '/o-hibrido-online/',
  build: {
    outDir: 'public'
  }
})
